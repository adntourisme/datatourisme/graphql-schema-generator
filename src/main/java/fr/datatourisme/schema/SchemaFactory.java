/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.schema;

import org.apache.jena.ontology.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerFactory;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by blaise on 08/07/17.
 */
public class SchemaFactory {
    OntModel ontModel;

    public SchemaFactory(List<String> files) {
        // disable imports
        OntDocumentManager dm = OntDocumentManager.getInstance();
        dm.setProcessImports(false);

        // prepare spec
        OntModelSpec spec = new OntModelSpec(ModelFactory.createMemModelMaker(), null, new CustomReasonerFactory(), ProfileRegistry.OWL_LANG);

        // build model
        ontModel = ModelFactory.createOntologyModel(spec);
        for(String input: files) {
            RDFDataMgr.read(ontModel, input) ;
        }
    }

    /**
     * Create the schema from the ontology
     *
     * @return
     */
    public Schema createSchema() {
        Schema schema = new Schema("datatourisme");

        Map<String, String> prefixMap = ontModel.getNsPrefixMap();
        if(prefixMap.get("") != null) {
            prefixMap.put("__empty__", prefixMap.get(""));
            prefixMap.remove("");
        }
        schema.setPrefixes(prefixMap);

        // datatypes
//        ontModel.listStatements(null, RDF.type, RDFS.Datatype)
//            .mapWith(Statement::getSubject)
//            .filterDrop(RDFNode::isAnon)
//            .forEachRemaining(r -> {
//                Type type = new Type();
//                type.setType(Type.TYPE.scalar);
//                schema.addType(shortForm(r.getURI()), type);
//            }
//        );

        // populate object types
        listNamedClasses().forEach(c -> {
            Type type = new Type();
            type.setName(getLabel(c));
            type.setDescription(getComment(c));

            listCandidateProperties(c)
                .forEach(p -> {

                    List<String> ranges = listRanges(p)
                        .map(r -> shortForm(r.getURI()))
                        .collect(Collectors.toList());

                    if(ranges.size() > 0) {
                        String uri = shortForm(p.getURI());

                        schema.getFields().computeIfAbsent(uri, s -> {
                            Field field = new Field();
                            if(ranges.size() == 1) {
                                field.setType(ranges.get(0));
                            } else {
                                field.setType(ranges);
                            }
                            field.setName(getLabel(p));
                            field.setDescription(getComment(p));
                            return field;
                        });

                        type.addField(uri);
                    }
                });

            if(type.getFields().size() > 0) {
                schema.addType(shortForm(c.getURI()), type);
            }
        });

        return schema;
    }

    /**
     * list named classes
     *
     * @return
     */
    private Stream<OntClass> listNamedClasses() {
        return ontModel.listNamedClasses().toList().stream()
            .filter(ontClass -> !ontClass.isAnon());
    }

    /**
     * list direct properties
     *
     * @param res
     * @return
     */
    private Stream<OntProperty> listDirectProperties(Resource res) {
        return ontModel.listAllOntProperties().toList().stream()
            .filter(p -> {
                return p.listDomain().toList().stream()
                    //.filter(OntResource::isClass) // remove protection to detect anomalies
                    .map(OntResource::asClass)
                    .flatMap(c -> {
                        // if the domain is an union class, flat map operands
                        if(c.isUnionClass()) {
                            //return c.asUnionClass().listOperands().toList().stream();
                            return c.asUnionClass().getOperands().iterator()
                                .filterKeep(RDFNode::isResource)
                                .mapWith(RDFNode::asResource).toList().stream();
                        }
                        return Stream.of(c);
                    })
                    .filter(c -> !c.isAnon())
                    .anyMatch(c -> {
                        return c.equals(res)
                                || c.canAs(OntClass.class) && c.as(OntClass.class).hasEquivalentClass(res)
                                || c.equals(OWL.Thing);
                    });
            });
    }

    /**
     * list direct superclasses
     *
     * @param res
     * @return
     */
    private Stream<Resource> listSuperClasses(Resource res, Boolean direct) {
        if(direct) {
//        return res.listSuperClasses(true).toList().stream()
//                .filter(c -> !c.isAnon());
            return res.listProperties(RDFS.subClassOf)
                    .mapWith(Statement::getObject)
                    .filterKeep(o -> !o.isAnon()/* && o.canAs(OntClass.class)*/)
                    .mapWith(RDFNode::asResource)
                    .toList().stream();
        } else {
            return Stream.concat(
                listSuperClasses(res, true),
                listSuperClasses(res, true).flatMap(c -> listSuperClasses(c, false))
            );
        }
    }

    /**
     * list direct subclasses
     *
     * @param res
     * @return
     */
    private Stream<Resource> listSubClasses(Resource res, Boolean direct) {
        if(direct) {
//          return res.listSubClasses(true).toList().stream()
//                .filter(c -> !c.isAnon());
            return ontModel.listStatements(null, RDFS.subClassOf, res)
                    .mapWith(Statement::getSubject)
                    .filterKeep(o -> !o.isAnon())
                    .mapWith(RDFNode::asResource)
                    .toList().stream();
        } else {
            return Stream.concat(
                listSubClasses(res, true),
                listSubClasses(res, true).flatMap(c -> listSubClasses(c, false))
            );
        }
    }

    /**
     * list candidate properties
     *
     * @return
     */
    private Stream<OntProperty> listCandidateProperties(OntClass res) {
        return Stream.concat(
            listDirectProperties(res),
            Stream.concat(
                listSuperClasses(res, false).flatMap(this::listDirectProperties),
                listSubClasses(res, false).flatMap(_c -> Stream.concat(
                    listDirectProperties(_c),
                    listSuperClasses(_c, false).flatMap(this::listDirectProperties)
                ))
            )).distinct();
    }

    /**
     * list ranges
     *
     * @return
     */
    private Stream<OntClass> listRanges(OntProperty property) {
        return property.listRange().toList().stream()
            //.filter(OntResource::isClass)
            .map(OntResource::asClass)
            .flatMap(c -> {
                // if the range is an union class, flat map operands
                //return c.asUnionClass().listOperands().toList().stream();
                if(c.isUnionClass()) {
                    return c.asUnionClass().getOperands().iterator()
                            .filterKeep(n -> n.canAs(OntClass.class))
                            .mapWith( n -> n.as( OntClass.class ) ).toList().stream();
                }
                return Stream.of(c);
            })
            .filter(c -> !c.isAnon());
    }

    /**
     * short uri form
     *
     * @return
     */
    private String shortForm(String uri) {
        return ontModel.shortForm(uri);
    }

    /**
     * get comment, in french first
     *
     * @return
     */
    private String getComment(OntResource res) {
        String comment = res.getComment("fr");
        if(comment == null) {
            comment = res.getComment(null);
        }
        return comment;
    }

    /**
     * get label, in french first
     *
     * @return
     */
    private String getLabel(OntResource res) {
        String label = res.getLabel("fr");
        if(label == null) {
            label = res.getLabel(null);
        }
        return label;
    }
    /**
     *
     */
    class CustomReasonerFactory implements ReasonerFactory {
        @Override
        public Reasoner create(Resource configuration) {
            return new GenericRuleReasoner(Rule.rulesFromURL("rules.txt"));
        }

        @Override
        public Model getCapabilities() {
            return null;
        }

        @Override
        public String getURI() {
            return null;
        }
    }
}
